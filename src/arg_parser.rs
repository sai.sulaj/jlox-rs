use clap::{Arg, App};

pub enum ArgResult {
    Prompt,
    File(String),
}

pub fn parse_args() -> ArgResult {
    let raw = App::new("jlox-rs")
        .version("0.1")
        .author("Saimir Sulaj <hola@saimirsulaj.com>")
        .about("Rust implementation of the jlox interpreter")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .required(false)
                .help("File path")
        )
        .get_matches();

    if let Some(filename) = raw.value_of("file") {
        return ArgResult::File(filename.to_owned());
    };

    ArgResult::Prompt
}
