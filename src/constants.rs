use std::{
    collections::HashMap,
    fmt,
};

macro_rules! define_reserved {
    ($($s:ident = $t:ident),*) => {{
        let mut reserved = HashMap::new();

        $(reserved.insert(stringify!($s).to_owned(), Token::$t);)*

        reserved
    }}
}

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    SemiColon,
    Slash,
    Star,
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,
    Str(String),
    Number(f64),
    Identifier,
    And,
    Class,
    Else,
    False,
    Fun,
    For,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,
    Eof,
}

pub enum LiteralKind {
    Number(f64),
    Boolean(bool),
    String(String),
    None,
}
impl fmt::Display for LiteralKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LiteralKind::Number(num) => write!(f, "Literal::Number({})", num),
            LiteralKind::Boolean(b) => write!(f, "Literal::Boolean({})", b),
            LiteralKind::String(s) => write!(f, "Literal::String({})", s),
            LiteralKind::None => write!(f, "Literal::None"),
        }
    }
}

pub fn get_reserved() -> HashMap<String, Token> {
    let reserved = define_reserved!(
        and = And,
        class = Class,
        else = Else,
        false = False,
        for = For,
        fun = Fun,
        if = If,
        nil = Nil,
        or = Or,
        print = Print,
        return = Return,
        super = Super,
        this = This,
        true = True,
        var = Var,
        while = While
    );

    reserved
}
