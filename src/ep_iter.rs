use std::{
    str,
    collections,
    iter,
};

pub struct EPIter<'a> {
    iter: str::Chars<'a>,
    queue: collections::VecDeque<Option<char>>,
}

impl<'a> EPIter<'a> {
    pub fn new(iter: str::Chars<'a>) -> Self {
        EPIter {
            iter,
            queue: collections::VecDeque::new(),
        }
    }

    pub fn peek(&mut self, num: usize) -> Option<char> {
        let queue_len = self.queue.len();

        if queue_len < num {
            for _ in 0..(num - queue_len) {
                self.queue.push_front(self.iter.next());
            }

            self.queue.front().unwrap().clone()
        } else {
            self.queue.get(queue_len - num).unwrap().clone()
        }
    }
}

impl<'a> iter::Iterator for EPIter<'a> {
    type Item = char;

    fn next(&mut self) -> Option<char> {
        if self.queue.is_empty() {
            self.iter.next()
        } else {
            self.queue.pop_back().unwrap()
        }
    }
}
