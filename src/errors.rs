use std::fmt;

pub struct TokenizerError {
    message: String,
    line: usize,
}
impl TokenizerError {
    pub fn new(message: String, line: usize) -> Self {
        TokenizerError {
            message,
            line,
        }
    }
}
impl fmt::Display for TokenizerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[line {}] Error: {}", self.line, self.message)
    }
}
