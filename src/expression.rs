use crate::constants::{
    Token,
    LiteralKind,
};

macro_rules! define_ast {
    ($($c_name:ident: [$($field:ident $type:ty),*])*) => {
        pub mod ast {
            use crate::constants::{Token, LiteralKind};

            pub enum Expr {
                $($c_name {
                    $($field: $type,)*
                },)*
            }
        }
    };
}

define_ast!(
    Binary: [left Box::<Expr>, operator Token, right Box::<Expr>]
    Grouping: [expr Box::<Expr>]
    Unary: [operator Token, right Box::<Expr>]
    Literal: [value LiteralKind]
);

pub mod visit {
    use crate::expression::ast::*;

    pub trait Visitor<T> {
        fn visit_expr(&mut self, data: &Expr) -> T;
    }
}

use visit::*;
use ast::*;

pub struct Interpreter;
impl Visitor<String> for Interpreter {
    fn visit_expr(&mut self, data: &Expr) -> String {
        match *data {
            Expr::Binary { ref left, ref operator, ref right } => {
                format!("({:?} {} {})", operator, self.visit_expr(left), self.visit_expr(right))
            }
            Expr::Grouping { ref expr } => {
                format!("({})", self.visit_expr(expr))
            }
            Expr::Unary { ref operator, ref right } => {
                format!("({:?} {})", operator, self.visit_expr(right))
            }
            Expr::Literal { ref value } => {
                format!("{}", value)
            }
        }
    }
}
