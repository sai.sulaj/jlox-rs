use std::{
    fs,
    error,
    io,
    io::Write,
};

mod tokenizer;
mod constants;
mod arg_parser;
mod errors;
mod ep_iter;
mod expression;

use constants::{LiteralKind, Token};
use expression::{
    ast,
    Interpreter,
    visit::Visitor,
};
use tokenizer::Tokenizer;
use arg_parser::{
    parse_args,
    ArgResult,
};

fn run(source: &str) {
    let tokens =match Tokenizer::new(source).scan_tokens() {
        Ok(toks) => toks,
        Err(err) => {
            println!("{}", err);
            Vec::new()
        },
    };

    for tok in tokens {
        println!("{:?}", tok);
    }
}

fn run_file(filename: &str) -> Result<(), Box<dyn error::Error + 'static>> {
    let source: String = fs::read_to_string(filename)?;
    run(&source);

    Ok(())
}

fn run_prompt() -> Result<(), Box<dyn error::Error + 'static>> {
    let mut line: String = String::new();
    
    loop {
        print!("> ");
        io::stdout().flush()?;
        io::stdin().read_line(&mut line)?;
        run(&line);
        line.clear();
    }
}

fn main() {
    // let args: ArgResult = parse_args();

    // let result = match args {
        // ArgResult::File(filename) => run_file(&filename),
        // ArgResult::Prompt => run_prompt(),
    // };

    // if let Err(err) = result {
        // println!("{}", err);
    // }

    let e = ast::Expr::Binary {
        left: Box::new(
            ast::Expr::Literal {
                value: LiteralKind::Number(1.)
            }
        ),
        operator: Token::Plus,
        right: Box::new(
            ast::Expr::Literal {
                value: LiteralKind::Number(2.)
            }
        ),
    };
    let mut interpreter = Interpreter{};
    let out = interpreter.visit_expr(&e);
    println!("{}", out);
}
