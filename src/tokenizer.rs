use std::{
    str,
    collections::HashMap,
};

use crate::{
    ep_iter::EPIter,
    constants::{
        Token,
        get_reserved,
    },
    errors::TokenizerError,
};

fn is_digit(c: char) -> bool {
    c >= '0' && c <= '9'
}

fn is_alpha(c: char) -> bool {
    (c >= 'a' && c <= 'z') ||
    (c >= 'A' && c <= 'Z') ||
    c == '_'
}

fn is_alphanumeric(c: char) -> bool {
    is_alpha(c) || is_digit(c)
}

type SrcIter<'a> = EPIter<'a>;
pub struct Tokenizer {
    source: String,
    line: usize,
}
impl Tokenizer {
    pub fn new(source: &str) -> Self {
        Tokenizer {
            source: source.to_owned(),
            line: 0,
        }
    }

    fn peek_match(&mut self, src_iter: &mut SrcIter, target: char) -> bool {
        let next: Option<char> = src_iter.peek(1);
        if next.is_none() { return false; }
        let next = next.unwrap();
        if next != target { return false; }

        src_iter.next();
        true
    }

    fn string(&mut self, src_iter: &mut SrcIter) -> Result<Token, TokenizerError> {
        let mut value = String::new();
        while let Some(c) = src_iter.peek(1) {
            if c == '"' { break; }
            if c == '\n' { self.line += 1; }
            value.push(c);
            src_iter.next();
        }

        // Unterminated string.
        if let None = src_iter.peek(1) {
            return Err(TokenizerError::new(format!("Terminate your strings dumbass"), self.line));
        }
        
        // The closing ".
        src_iter.next();
        Ok(Token::Str(value))
    }

    fn number(&mut self, src_iter: &mut SrcIter) -> f64 {
        let mut num = String::new();

        while let Some(c) = src_iter.peek(1) {
            if !is_digit(c) { break; }
            num.push(src_iter.next().unwrap());
        }

        // Look for fractional.
        let peek_1 = src_iter.peek(1);
        let peek_2 = src_iter.peek(2);
        if peek_1.is_some() && peek_2.is_some() && peek_1.unwrap() == '.' && is_digit(peek_2.unwrap()) {
            num.push(src_iter.next().unwrap());

            while let Some(d) = src_iter.peek(1) {
                if !is_digit(d) { break; }
                num.push(src_iter.next().unwrap());
            }
        }

        num.parse::<f64>().unwrap()
    }

    fn identifier(&mut self, reserved: &HashMap<String, Token>, src_iter: &mut SrcIter) -> Token {
        let mut string = String::new();

        while let Some(c) = src_iter.peek(1) {
            if !is_alphanumeric(c) { break; }
            string.push(src_iter.next().unwrap());
        }

        if reserved.contains_key(&string) {
            return reserved.get(&string).unwrap().clone();
        }

        Token::Identifier
    }

    pub fn scan_tokens(&mut self) -> Result<Vec::<Token>, TokenizerError> {
        let reserved = get_reserved();
        let source = self.source.clone();
        let src_chars = source.chars();
        let mut src_iter = EPIter::new(src_chars);
        let mut tokens: Vec::<Token> = Vec::new();

        while let Some(c) = src_iter.peek(1) {
            match c {
                '(' => {
                    tokens.push(Token::LeftParen);
                    src_iter.next();
                },
                ')' => {
                    tokens.push(Token::RightParen);
                    src_iter.next();
                },
                '{' => {
                    tokens.push(Token::LeftBrace);
                    src_iter.next();
                },
                '}' => {
                    tokens.push(Token::RightBrace);
                    src_iter.next();
                },
                ',' => {
                    tokens.push(Token::Comma);
                    src_iter.next();
                },
                '.' => {
                    tokens.push(Token::Dot);
                    src_iter.next();
                },
                '-' => {
                    tokens.push(Token::Minus);
                    src_iter.next();
                },
                '+' => {
                    tokens.push(Token::Plus);
                    src_iter.next();
                },
                ';' => {
                    tokens.push(Token::SemiColon);
                    src_iter.next();
                },
                '*' => {
                    tokens.push(Token::Star);
                    src_iter.next();
                },
                '!' => {
                    src_iter.next();
                    tokens.push(
                        if self.peek_match(&mut src_iter, '=') {
                            Token::BangEqual
                        } else {
                            Token::Bang
                        }
                    );
                },
                '=' => {
                    src_iter.next();
                    tokens.push(
                        if self.peek_match(&mut src_iter, '=') {
                            Token::EqualEqual
                        } else {
                            Token::Equal
                        }
                    );
                },
                '>' => {
                    src_iter.next();
                    tokens.push(
                        if self.peek_match(&mut src_iter, '=') {
                            Token::GreaterEqual
                        } else {
                            Token::Greater
                        }
                    );
                },
                '<' => {
                    src_iter.next();
                    tokens.push(
                        if self.peek_match(&mut src_iter, '=') {
                            Token::LessEqual
                        } else {
                            Token::Less
                        }
                    );
                },
                '/' => {
                    src_iter.next();
                    if self.peek_match(&mut src_iter, '/') {
                        while let Some(c_next) = src_iter.peek(1) {
                            if c_next != '\n' { break; }
                        }
                    } else {
                        tokens.push(Token::Slash);
                    }
                },
                ' ' | '\r' | '\t' => {
                    src_iter.next();
                },
                '\n' => {
                    self.line += 1;
                    src_iter.next();
                }
                '"' => {
                    src_iter.next();
                    let string_token = self.string(&mut src_iter)?;
                    tokens.push(string_token);
                },
                _ => {
                    if is_digit(c) {
                        let num = self.number(&mut src_iter);
                        tokens.push(Token::Number(num));
                    } else if is_alpha(c) {
                        let ident = self.identifier(&reserved, &mut src_iter);
                        tokens.push(ident);
                    } else {
                        return Err(TokenizerError::new(format!("Bruh wtf is this '{}'?", c), self.line));
                    }
                }
            };
        }

        tokens.push(Token::Eof);
        Ok(tokens)
    }
}
